# فرمان `sed`

از این فرمان برای نوشتن عبارت باقاعده بازاستفپذیر استفیده می‌شود. همانند اسکریپت bash که برای بازاستفیدن از مجموعه‌ای از فرمان‌ها استفیده می‌شود. برای این کافیه عبارت‌های باقاعده‌امان را خط‌به‌خط در یه فایل با ساختار زیر بنویسیم.

```sed
#!/usr/bin/sed

# RegExs
```

**توجه.** خط نخست باید مسیر مفسر `sed` باشد که با دستور زیر می‌توان یافت‌اش:

```shell
which sed
```

و سپس به شکل زیر باجراییم:

```shell
sed -E -f <path_to_sed_file> <path_to_source_file>
```

**مثال.**

1. ساختن اسکریپت:

   <div dir="ltr" align="left" markdown="1">

   ```sed
    #!/usr/bin/sed
    
    s/^\s+//
    1s/(.+?):/RUN REPORT: \1\n/
    ```

   </div>

2. اجراییدن اسکریپت‌اِ:

    <div dir="ltr" align="left" markdown="1">

    ```bash
    sed -E -f <path_to_sed_file> <path_to_source_file>
    ```

    </div>
