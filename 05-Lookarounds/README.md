# نگاه به پیرامون (look around)

برای درنظر گرفتن یه عبارت در پشت یا جلو یه قاعده از پیشنگرنده و پسنگرنده می‌استفیم.

## پیشنگریستن (lookahead)

برای درنظر گرفتن عبارت جلویین در قاعده به‌شکل:

* پینگریستن مثبت (positive lookahead): <span dir="ltr" markdown="1">`...(?=...)`</span>
  **مثال.** <span dir="ltr" markdown="1">`(\d{5})(?=-\d{4})`</span>:
  ![پیشنگریستن مثبت](assets/lookahead-positive.png)
* پینگریستن منفی (negative lookahead): <span dir="ltr" markdown="1">`...(?!...)`</span>
  **مثال.** <span dir="ltr" markdown="1">`linuxacademy\.com(?!\/blog)`</span>
  ![پیشنگریستن منفی](assets/lookahead-negative.png)

## پسنگریستن (lookbehind)

برای درنظر گرفتن عبارت پشتین در قاعده به‌شکل:

* پسنگریستن مثبت (positive lookbehind): <span dir="ltr" markdown="1">`(?<=...)...`</span>
  **مثال.** <span dir="ltr" markdown="1">`(?<=<li><cod>).+(?=</cod>)`</span>
  ![پسنگریستن مثبت](assets/lookbehind-positive.png)
* پسنگریستن منفی (negative lookbehind): <span dir="ltr" markdown="1">`(?<!...)...`</span>
  **مثال.** <span dir="ltr" markdown="1">`(?<=http:\/\/).+`</span>
  ![پسنگریستن منفی](assets/lookbehind-negative.png)

**توجه.** طول عبارت پشتین باید ثابت باشد.

![پسنگریستن-طول ثابت](assets/lookbehind-fixed-length.png)
