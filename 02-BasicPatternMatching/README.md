# الگوهای پایه

به جای تعیین صریح یه حرف (character) می‌توان جانگهدار یه حرف از یه مجموعه را تعینید. برای تعریف یه مجموعه از `[]` (مانند، `[a-zA-Z0-9]`) می‌استفیم.

## مجموعه‌های گوهری

برخی از مجموعه‌های پرکاربردی که تعریف شده‌اند:

<div dir="rtl" align="right" markdown="1">

* `.`: جانگهدار هر حرفی
* `\w`: جانگهدار یه حرف الفبا-عددی (کوچک و بزرگ)
* `\W`: جانگهدار یه حرف غیر الفبا-عددی
* `\d`: جانگهدار یه عدد
* `\D`: جانگهدار یه غیر عدد
* `\s`: جانگهدار فضای‌سفید (whitespace)
* `\S`: جانگهدار غیر فضای‌سفید
* `\t`: جانگهدار tab
* `\n`: جانگهدار خط‌جدید
* `\r`: جانگهدار حرف برگشت

</div>

**توجه.** برای رهایاندن معنی حرف‌های (character) خاص کافی پشتشان حرف `\` نهیم:

```shell
grep "192\.168\.1\.1" log-url
```

**توجه.** برای تعیین تکرار می‌توان از فرانویسه‌ی `+` به‌جای تکرار چندباره استفید.

## استفیدن حرف‌های غیر انگلیسی

هنگام نیاز به استفیدن از حرف‌های غیر انگلیسی از یونیکد می‌استفیم. که البته در بعضی از موتورهای nginx سازگار است. در پایتون، جاوا، دات‌نت، و ایکس‌ام‌ال می‌توان از یونیکد استفید.

**مثال.**

```regex
ژ \u0698
پ \u067E
چ \u0686
گ \u06AF
```
