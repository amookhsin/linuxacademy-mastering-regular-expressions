# عبارت شرطی `if-else`

شرطیدن یه عبارت به یه گروه ستانی (Capturing Group) که به نحو <span dir="ltr" markdown="1">`(CAPTURING_GROUP_1|(CAPTURING_GROUP_2))(?(group_id)TRUE_CONDITION|FALSE_CONDITION)`</span> نوشته می‌شود.

**مثال.** عبارت باقاعده <span dir="ltr" markdown="1">`(ID|(FUNCTION)): (?(2)\w+|\d+)`</span> با عبارت‌های <span dir="ltr" markdown="1">`ID: 12345`</span> و <span dir="ltr" markdown="1">`FUNCTION: service`</span> جور است.

## استفیدن از گروه بانام

از نام یه گروه نیز می‌توان در عبارت شرطی به‌شکل <span dir="ltr" markdown="1">`(ID|(?<fn>FUNCTION)): (?(fn)\w+|\d+)`</span> استفید.

## لانیدن عبارت‌های شرطی درهم

می‌توان عبارت‌های شرطی را درهم لاناند. به‌عبارتی در بخش «مگر» (else) یه عبارت شرطی می‌توان یه عبارت شرطی دیگر نوشت.

**مثال.** <span dir="ltr" markdown="1">`(ID|(?<fn>FUNCTION)|(?<dur>DURATION)): (?(fn)\w+|(?(dur)\d+ ms|\d+))`</span> که با رشته‌های <span dir="ltr" markdown="1">`ID: 1234567`</span>، <span dir="ltr" markdown="1">`FUNCTION: func_name`</span>، و <span dir="ltr" markdown="1">DURATION: 1234 ms</span> جور است.
